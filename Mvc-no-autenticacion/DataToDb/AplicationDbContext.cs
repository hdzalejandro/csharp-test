﻿using System;
using Microsoft.EntityFrameworkCore;
using Mvc_no_autenticacion.Models;

namespace Mvc_no_autenticacion.DataToDb
{
    public class AplicationDbContext : DbContext
    {
        public AplicationDbContext(DbContextOptions<AplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Libro> Libro { get; set; }
    }
}
